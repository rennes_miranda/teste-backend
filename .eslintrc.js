module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
    'no-console': 0,
    'no-restricted-syntax': 0,
    'no-param-reassign': 0,
    'no-useless-escape': 0,
    camelcase: 0,
  },
};
