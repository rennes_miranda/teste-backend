const { Model, DataTypes } = require('sequelize');

class Rating extends Model {
  static init(sequelize) {
    super.init({
      rating: DataTypes.INTEGER,
      deletedAt: DataTypes.DATE,
    }, {
      sequelize,
    });
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'created_by', as: 'creator' });
    this.belongsTo(models.Movie, { foreignKey: 'movie_id', as: 'movieId' });
  }
}

module.exports = Rating;
