const { Model, DataTypes } = require('sequelize');

class Movie extends Model {
  static init(sequelize) {
    super.init({
      name: DataTypes.STRING,
      director: DataTypes.STRING,
      genre: DataTypes.STRING,
      deletedAt: DataTypes.DATE,
    }, {
      sequelize,
    });
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'created_by', as: 'creator' });
  }
}

module.exports = Movie;
