const { Model, DataTypes } = require('sequelize');

const PROTECTED_ATTRIBUTES = ['password'];

class User extends Model {
  toJSON() {
    const hidePassword = { ...this.get() };
    for (const attribute of PROTECTED_ATTRIBUTES) {
      delete hidePassword[attribute];
    }
    return hidePassword;
  }

  static init(sequelize) {
    super.init({
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      isAdmin: DataTypes.BOOLEAN,
      deletedAt: DataTypes.DATE,
    }, {
      sequelize,
    });
  }
}

module.exports = User;
