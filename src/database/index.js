const Sequelize = require('sequelize');
const dbConfig = require('../config/database');

const User = require('../models/User');
const Movie = require('../models/Movie');
const Rating = require('../models/Rating');

const connection = new Sequelize(dbConfig);

User.init(connection);
Movie.init(connection);
Rating.init(connection);

Movie.associate(connection.models);
Rating.associate(connection.models);

module.exports = connection;
