const Router = require('express');
const MovieController = require('../controllers/MovieController');
const checkAhthenticated = require('../middlewares/checkAuthenticated');

const moviesRouter = Router();

moviesRouter.post('/:id/movies', checkAhthenticated, MovieController.create);
moviesRouter.get('/:id/movies', checkAhthenticated, MovieController.list);
moviesRouter.get('/:id/movies/:movie_id', checkAhthenticated, MovieController.listOne);
moviesRouter.post('/:id/movies/:movie_id/ratings', checkAhthenticated, MovieController.createRating);

module.exports = moviesRouter;
