const Router = require('express');
const UserController = require('../controllers/UserController');

const usersRouter = Router();

usersRouter.post('/', UserController.create);
usersRouter.get('/', UserController.listAll);
usersRouter.get('/:id', UserController.listOne);
usersRouter.delete('/:id', UserController.delete);

module.exports = usersRouter;
