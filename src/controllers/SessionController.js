const { compare } = require('bcryptjs');
const { sign } = require('jsonwebtoken');
const User = require('../models/User');
require('dotenv').config();

module.exports = {
  async create(req, res) {
    const { email, password } = req.body;

    try {
      const user = await User.findOne({ where: { email } });

      if (!user) {
        throw new Error('Incorrect email/password combination.');
      }

      if (user.deletedAt !== null) {
        throw new Error('User deleted!');
      }

      const pwdCompare = await compare(password, user.password);

      if (!pwdCompare) {
        throw new Error('Incorrect email/password combination');
      }
      const token = sign(
        {},
        '713bdc095a882d9bb74c5d16ec8ca6d5'
        || process.env.JWT_SECRET,
        {
          subject: user.id,
          expiresIn: '1d',
        },
      );

      return res.json({ user, token });
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  },
};
