const { hash } = require('bcryptjs');
const { v4 } = require('uuid');
const User = require('../models/User');

module.exports = {
  async listAll(req, res) {
    const users = await User.findAll();

    return res.json(users);
  },

  async listOne(req, res) {
    const { id } = req.params;

    const user = await User.findOne({ where: { id } });
    return res.json(user);
  },

  async create(req, res) {
    const {
      name, email, password, isAdmin,
    } = req.body;

    try {
      const checkUser = await User.findOne({ where: { email } });

      const regexEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (!regexEmail.test(email)) {
        throw new Error('Invalid email format!');
      }

      if (checkUser) {
        throw new Error('Email address already used');
      }

      const pwdEncryption = await hash(password, 8);

      const user = await User.create({
        id: v4(),
        name,
        email,
        isAdmin,
        password: pwdEncryption,
      });

      return res.json(user);
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  },

  async delete(req, res) {
    const { id } = req.params;
    try {
      const user = await User.findOne({ where: { id } });
      console.log(user.dataValues.deletedAt);
      if (user.dataValues.deletedAt !== null) {
        throw new Error('User already deleted!');
      }

      await User.update(
        { deletedAt: new Date() },
        { where: { id } },
      );

      return res.json({ deleted: true });
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  },
};
