const { v4 } = require('uuid');
const User = require('../models/User');
const Movie = require('../models/Movie');
const Rating = require('../models/Rating');

module.exports = {
  async list(req, res) {
    const { name, director, genre } = req.query;
    console.log(req.query);

    const query = {
      ...(name && { name }),
      ...(director && { director }),
      ...(genre && { genre }),
    };

    const movies = await Movie.findAll({ where: query });

    return res.json(movies);
  },
  async listOne(req, res) {
    const { movie_id } = req.params;

    const movie = await Movie.findOne({ where: { id: movie_id } });
    const ratingArray = await Rating.findAll({ where: { movie_id } });

    if (ratingArray.length) {
      const ratings = ratingArray.map((result) => result.rating);

      const reducer = (accumulator, currentValue) => accumulator + currentValue;
      const sum = ratings.reduce(reducer);
      const average = sum / ratings.length;

      const data = {
        ...movie.dataValues,
        ...(ratings.length && { average }),
      };

      return res.json(data);
    }

    return res.json(movie);
  },
  async create(req, res) {
    const { name, director, genre } = req.body;
    const { id } = req.params;

    try {
      const user = await User.findOne({ where: { id } });
      if (user.isAdmin === false) {
        throw new Error('You must be an administrator!');
      }
      const movie = await Movie.create({
        id: v4(),
        name,
        director,
        genre,
        created_by: id,
      });

      return res.json({ movie });
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  },
  async createRating(req, res) {
    const { rating } = req.body;
    const { id, movie_id } = req.params;
    try {
      const user = await User.findOne({ where: { id } });
      console.log('aqui');
      if (user.isAdmin === true) {
        throw new Error('You must be an user!');
      }

      if (rating >= 0 && rating <= 4) {
        const response = await Rating.create({
          id: v4(),
          rating,
          created_by: id,
          movie_id,
        });

        return res.json(response);
      }
      throw new Error('Rating must be between 0 and 4');
    } catch (err) {
      return res.status(400).json({ error: err.message });
    }
  },
};
