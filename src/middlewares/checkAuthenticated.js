const { verify } = require('jsonwebtoken');
require('dotenv').config();

module.exports = function checkAuthenticated(req, res, next) {
  const authHeader = req.headers.authorization;

  if (!authHeader) {
    throw new Error('JWT token is missing');
  }

  const [, token] = authHeader.split(' ');

  try {
    verify(token, '713bdc095a882d9bb74c5d16ec8ca6d5'
    || process.env.JWT_SECRET);

    return next();
  } catch (err) {
    throw new Error('Invalid JWT token');
  }
};
