require('dotenv').config();

module.exports = {
  dialect: 'postgres',
  host: 'localhost' || process.env.POSTGRES_HOST,
  username: 'postgres' || process.env.POSTGRES_USERNAME,
  password: 'docker' || process.env.POSTGRES_PASSWORD,
  database: 'sqlnode' || process.env.POSTGRES_DATABASE,
  define: {
    timestamps: true,
    underscored: true,
  },
};
